class Board
  attr_reader :grid

  def initialize grid = self.class.default_grid
    @grid = grid
  end

  def self.default_grid
    Array.new(10) {Array.new(10)}
  end

  def[](pos)
    row, col = pos
    @grid[row][col]
  end

  def[]=(pos,mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def count
    counter = 0
    cell_doer { |cell| counter += 1 if cell != nil}
    counter
  end

  def empty? pos=nil
    if pos
      return !self[pos]

    else
      cell_doer { |cell| return false if cell != nil}
    end
  end

  def full?
    cell_doer { |cell| return false if cell == nil}
    true
  end

  def place_random_ship
    if full?
      raise "The seas are packed me hearties!" if full?
    else
      pos = [rand(grid.count),rand(grid.count)]
      self[pos] = :s
    end
  end

  def won?
    empty?
  end

  #Helper method
  def cell_doer(&prc)
    grid.each do |row|
      row.each { |cell| prc.call(cell) }
    end
  end

  def mark pos, mark
    self[pos] = mark
  end

end
